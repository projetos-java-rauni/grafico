/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pss.grafico.presenter;

import com.pss.grafico.model.BuildGrafico;
import com.pss.grafico.model.Diretor;
import com.pss.grafico.model.MostrarGrafico;
import com.pss.grafico.model.decorators.DecoratorCores;
import com.pss.grafico.model.decorators.DecoratorLegendas;
import com.pss.grafico.model.decorators.DecoratorTitulo;
import com.pss.grafico.view.*;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class MainPresenter {
    private MainView view;

    public MainPresenter(){try{
    view = new MainView();
      
      Diretor criar=new Diretor();
      BuildGrafico grafico= new BuildGrafico();
      criar.GraficoHorizontal(grafico);
        
        MostrarGrafico g =grafico.getGrafico();
    
        
      refresh(view,g);
        
     view.getBtnFechar().addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent arg0) {
             view.dispose();
         }
     });
     
     view.getPadraoBox().addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent arg0) {
             if(view.getPadraoBox().getSelectedIndex()==1){ 
                 criar.GraficoVertical(grafico);
                 MostrarGrafico g =grafico.getGrafico();
                 
             }
             else {
                 criar.GraficoHorizontal(grafico);
                  MostrarGrafico g =grafico.getGrafico();
              
             }
         }
     });
     
     view.getBtnRestaurar().addActionListener(new ActionListener() {
         @Override
        public void actionPerformed(ActionEvent arg0) {
            MostrarGrafico g =grafico.getGrafico();
            refresh(view,g); 
        }
     });
    
     view.getTituloBox().addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent arg0) {
            if(view.getTituloBox().isSelected()==true){
                var a=new DecoratorTitulo(g);
                refresh(view, g);
            }
        }
    });
     view.getLegendaBox().addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent arg0) {
            if(view.getLegendaBox().isSelected()==true){
                var b=new DecoratorLegendas(g);
                refresh(view, g);
            }
        }
    });
     view.getRotuloCorBox().addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent arg0) {
            if(view.getRotuloCorBox().isSelected()==true){
                var c=new DecoratorCores(g);
                refresh(view, g);
            }
        }
    });
     
    }
 catch (Exception ex){
     System.out.println("erro \n com.pss.grafico.presenter.MainPresenter.<init>()");
     return;
 }
            
        
    }
    public MainView getView() {
        return view;
    }
    public void refresh(MainView view, MostrarGrafico g){
     view.getjChart().removeAll();
	view.getjChart().setLayout(new BorderLayout());
	view.getjChart().add(g.getPainel());
	view.pack();   
    }

   public static void main(String[] args){
   new MainPresenter();
    }

    
}

