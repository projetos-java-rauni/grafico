/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pss.grafico.model;

import org.jfree.chart.plot.PlotOrientation;

/**
 *
 * @author UFES
 */
public class BuildGrafico implements BuilderGrafico{
    private PlotOrientation orientacao;

    public void setHorizontal() {
        this.orientacao = PlotOrientation.HORIZONTAL;
    }

    public void setVertical() {
        this.orientacao = PlotOrientation.VERTICAL;
    }
    public MostrarGrafico getGrafico(){
        return new MostrarGrafico(orientacao);
    }
}
