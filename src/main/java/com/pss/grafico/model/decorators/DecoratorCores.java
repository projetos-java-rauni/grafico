/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pss.grafico.model.decorators;

import com.pss.grafico.model.MostrarGrafico;
import java.awt.Color;
import org.jfree.chart.plot.CategoryPlot;

/**
 *
 * @author UFES
 */
public class DecoratorCores {

    public DecoratorCores(MostrarGrafico g) {
    
    CategoryPlot plot = g.getGrafico().getCategoryPlot();
        plot.getRenderer().setSeriesPaint(0, Color.BLUE);
        plot.getRenderer().setSeriesPaint(1, Color.PINK);
  
    }
}
